<div align="center">
  <img src="assets/logo.png" height="124px"/><br/>
  <h1>Assignment 2</h1>
  <p>Repository with the Android application for the Assignment 2</p>
</div>


## 📝 About The Project

This repository contains an Android app, which is "Tinder with Chuck Norris". The app shows one joke about Chuck Norris at a time that changes by tapping a button. The backend is [api.chucknorris.io](https://api.chucknorris.io/).

A list of features and a the app demo can be found in our wiki: [list of features](https://gitlab.com/markovvn1-iu/f22-ccmdwf/assignment-2/-/wikis/Application-features), [demo](https://gitlab.com/markovvn1-iu/f22-ccmdwf/assignment-2/-/wikis/Applicaton-demo)

## :rocket: Try App!

Try our application using latest build artifact: :link: [Download latest APK for Android](https://gitlab.com/markovvn1-iu/f22-ccmdwf/assignment-2/-/jobs/artifacts/main/file/assignment2.apk?job=build)

### ⚙️ Developing

1. Install Android Studio IDE or just use VS Code
2. [Install Dart and Flutter](https://flutter.dev/docs/get-started/install)
3. Clone project `git clone https://gitlab.com/markovvn1-iu/f22-ccmdwf/assignment-2.git`
4. Open terminal in the directory of the project
5. `flutter pub get`
6. Download Android emulator [For VS Code](https://flutteragency.com/setup-emulator-for-vscode/)/[For Android Studio](https://developer.android.com/studio/run/managing-avds)
7. `flutter run`

## :computer: Contributors

<p>

  :mortar_board: <i>All participants in this project are undergraduate students in the <a href="https://apply.innopolis.university/en/bachelor/">Department of Computer Science</a> <b>@</b> <a href="https://innopolis.university/">Innopolis University</a></i> <br> <br>

  :boy: <b>Vladimir Markov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>Markovvn1@gmail.com</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitLab: <a href="https://gitlab.com/markovvn1">@markovvn1</a> <br>
</p>