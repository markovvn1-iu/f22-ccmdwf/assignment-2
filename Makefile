.PHONY: lint
lint:
	flutter format lib --line-length=120 --set-exit-if-changed
	flutter analyze


.PHONY: format
format:
	flutter format lib --line-length=120


.PHONY: generate
generate:
	flutter pub run build_runner build


.PHONY: build
build:
	flutter build apk
