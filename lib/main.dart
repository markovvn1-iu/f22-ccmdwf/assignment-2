import 'package:assignment2/locator.dart';
import 'package:assignment2/routes.dart';
import 'package:assignment2/screens/about.dart';
import 'package:assignment2/screens/favorites.dart';
import 'package:assignment2/screens/random_joke.dart';
import 'package:assignment2/screens/search.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  // Pass all uncaught errors from the framework to Crashlytics.
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterFatalError;

  initLocator();
  runApp(const MyApp());
}

class MyScreen extends StatelessWidget {
  const MyScreen({super.key, required this.title, required this.route, required this.body});

  final String title;
  final String route;
  final Widget body;

  void _onTab(BuildContext context, String targetRote) {
    if (route != targetRote) {
      Navigator.popAndPushNamed(context, targetRote);
    } else {
      Navigator.pop(context);
    }
  }

  Widget _buildDrawer(BuildContext context) {
    return Drawer(
      width: 250,
      child: ListView(
        children: [
          DrawerHeader(
            child: Container(
              margin: const EdgeInsets.all(10),
              child: const Image(
                image: AssetImage('assets/chucknorris_logo.png'),
              ),
            ),
          ),
          ListTile(
            title: const Text("Random joke"),
            leading: const Icon(Icons.casino_outlined),
            onTap: () => _onTab(context, Routes.randomJokes),
          ),
          ListTile(
            title: const Text("Search"),
            leading: const Icon(Icons.search),
            onTap: () => _onTab(context, Routes.search),
          ),
          ListTile(
            title: const Text("My favorites"),
            leading: const Icon(Icons.favorite_outline),
            onTap: () => _onTab(context, Routes.favorites),
          ),
          ListTile(
            title: const Text("About"),
            leading: const Icon(Icons.info_outline),
            onTap: () => _onTab(context, Routes.about),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      drawer: _buildDrawer(context),
      body: body,
    );
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Assignment 2',
      theme: ThemeData(
          brightness: Brightness.light,
          primarySwatch: Colors.blue,
          elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            ),
          )),
      initialRoute: Routes.randomJokes,
      routes: {
        Routes.randomJokes: (_) =>
            const MyScreen(title: "Random jokes", route: Routes.randomJokes, body: RandomJokePage()),
        Routes.favorites: (_) => const MyScreen(title: "Favorites", route: Routes.favorites, body: FavoritePage()),
        Routes.search: (_) => const MyScreen(title: "Search", route: Routes.search, body: SearchPage()),
        Routes.about: (_) => const MyScreen(title: "About", route: Routes.about, body: AboutPage()),
      },
    );
  }
}
