class Routes {
  static const randomJokes = "/";
  static const favorites = "/favorites";
  static const about = "/about";
  static const search = "/search";
}
