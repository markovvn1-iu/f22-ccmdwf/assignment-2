// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chucknorris.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChuckNorrisJoke _$ChuckNorrisJokeFromJson(Map<String, dynamic> json) => ChuckNorrisJoke(
      (json['categories'] as List<dynamic>).map((e) => e as String).toList(),
      json['icon_url'] as String,
      json['id'] as String,
      json['url'] as String,
      json['value'] as String,
    );

Map<String, dynamic> _$ChuckNorrisJokeToJson(ChuckNorrisJoke instance) => <String, dynamic>{
      'categories': instance.categories,
      'icon_url': instance.iconUrl,
      'id': instance.id,
      'url': instance.url,
      'value': instance.value,
    };

ChuckNorrisSearchResponse _$ChuckNorrisSearchResponseFromJson(Map<String, dynamic> json) => ChuckNorrisSearchResponse(
      (json['result'] as List<dynamic>).map((e) => ChuckNorrisJoke.fromJson(e as Map<String, dynamic>)).toList(),
    );

Map<String, dynamic> _$ChuckNorrisSearchResponseToJson(ChuckNorrisSearchResponse instance) => <String, dynamic>{
      'result': instance.result,
    };
