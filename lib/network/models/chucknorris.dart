import 'package:json_annotation/json_annotation.dart';

part 'chucknorris.g.dart';

@JsonSerializable()
class ChuckNorrisJoke {
  final List<String> categories;
  @JsonKey(name: 'icon_url')
  final String iconUrl;
  final String id;
  final String url;
  final String value;

  ChuckNorrisJoke(this.categories, this.iconUrl, this.id, this.url, this.value);

  factory ChuckNorrisJoke.fromJson(Map<String, dynamic> json) => _$ChuckNorrisJokeFromJson(json);

  Map<String, dynamic> toJson() => _$ChuckNorrisJokeToJson(this);
}

@JsonSerializable()
class ChuckNorrisSearchResponse {
  final List<ChuckNorrisJoke> result;

  ChuckNorrisSearchResponse(this.result);

  factory ChuckNorrisSearchResponse.fromJson(Map<String, dynamic> json) => _$ChuckNorrisSearchResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ChuckNorrisSearchResponseToJson(this);
}
