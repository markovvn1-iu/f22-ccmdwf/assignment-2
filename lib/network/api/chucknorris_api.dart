import 'package:assignment2/network/models/chucknorris.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';

part 'chucknorris_api.g.dart';

@RestApi(baseUrl: "https://api.chucknorris.io/jokes")
abstract class ChuckNorrisApi {
  factory ChuckNorrisApi(Dio dio, {String baseUrl}) = _ChuckNorrisApi;

  @GET("/random")
  Future<ChuckNorrisJoke> random({@Query('category') String? category});

  @GET("/categories")
  Future<List<String>> categories();

  @GET("/search")
  Future<ChuckNorrisSearchResponse> search(@Query('query') String query);
}
