class ChuckNorrisException implements Exception {}

class UnknownApiException implements ChuckNorrisException {}

class ConnectionFailedException implements ChuckNorrisException {}
