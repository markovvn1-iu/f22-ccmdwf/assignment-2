import 'dart:async';

import 'package:assignment2/network/api/chucknorris_api.dart';
import 'package:assignment2/network/exceptions/chucknorris_exceptions.dart';
import 'package:assignment2/network/models/chucknorris.dart';
import 'package:dio/dio.dart';

class ChuckNorrisRepository {
  ChuckNorrisRepository(this.api);

  final ChuckNorrisApi api;

  Future<ChuckNorrisJoke> random(List<String> categories) async {
    try {
      return await (categories.isEmpty ? api.random() : api.random(category: categories.join(",")));
    } catch (err) {
      if (err is DioError) {
        if (err.response == null) throw ConnectionFailedException();
      }
    }
    throw UnknownApiException();
  }

  Future<List<String>> categories() async {
    try {
      return await api.categories();
    } catch (err) {
      if (err is DioError) {
        if (err.response == null) throw ConnectionFailedException();
      }
    }
    throw UnknownApiException();
  }

  Future<List<ChuckNorrisJoke>> search(String query) async {
    try {
      return (await api.search(query)).result;
    } catch (err) {
      if (err is DioError) {
        if (err.response == null) throw ConnectionFailedException();
      }
    }
    throw UnknownApiException();
  }
}
