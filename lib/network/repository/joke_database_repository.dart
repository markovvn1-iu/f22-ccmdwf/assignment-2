import 'dart:async';

import 'package:assignment2/network/models/chucknorris.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';

class JokeDatabaseRepository {
  final Box? _database = null;

  Future<Box> getDatabase() async {
    if (_database != null) return _database!;

    if (!Hive.isBoxOpen('favoriteJokes')) {
      Hive.init((await getApplicationDocumentsDirectory()).path);
    }
    return await Hive.openBox('favoriteJokes');
  }

  Future<void> put(ChuckNorrisJoke joke) async {
    final Box db = await getDatabase();
    return await db.put(joke.id, joke.toJson());
  }

  Future<void> remove(String id) async {
    final Box db = await getDatabase();
    return await db.delete(id);
  }

  Future<bool> has(String id) async {
    final Box db = await getDatabase();
    return db.containsKey(id);
  }

  Stream<ChuckNorrisJoke> getAll() async* {
    final Box db = await getDatabase();
    for (String id in db.keys) {
      yield ChuckNorrisJoke.fromJson(Map<String, dynamic>.from(await db.get(id)));
    }
  }
}
