import 'package:assignment2/bloc/favorites/favorites_bloc.dart';
import 'package:assignment2/bloc/random_joke/random_joke_bloc.dart';
import 'package:assignment2/bloc/random_joke/random_joke_settings_bloc.dart';
import 'package:assignment2/bloc/search/search_bloc.dart';
import 'package:assignment2/network/api/chucknorris_api.dart';
import 'package:assignment2/network/repository/chucknorris_repository.dart';
import 'package:assignment2/network/repository/joke_database_repository.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';

final GetIt locator = GetIt.instance;

Future initLocator() async {
  Dio dio = Dio(BaseOptions(
    connectTimeout: 1000 * 5,
    receiveTimeout: 1000 * 5,
    sendTimeout: 1000 * 5,
  ));

  locator.registerSingleton(dio);

  locator.registerLazySingleton<ChuckNorrisApi>(() => ChuckNorrisApi(locator.get<Dio>()));
  locator.registerLazySingleton<ChuckNorrisRepository>(() => ChuckNorrisRepository(locator.get<ChuckNorrisApi>()));
  locator.registerLazySingleton<JokeDatabaseRepository>(() => JokeDatabaseRepository());

  locator.registerLazySingleton<RandomJokeBloc>(() {
    final bloc = RandomJokeBloc(locator<ChuckNorrisRepository>(), locator<JokeDatabaseRepository>());
    bloc.add(ShowNextJokeEvent());
    return bloc;
  });
  locator.registerLazySingleton<RandomJokeSettingsBloc>(() {
    final bloc = RandomJokeSettingsBloc(locator<ChuckNorrisRepository>());
    bloc.add(LoadSettingsEvent());
    return bloc;
  });
  locator.registerLazySingleton<FavoritesBloc>(() {
    final bloc = FavoritesBloc(locator<JokeDatabaseRepository>());
    bloc.add(ShowAllJokesEvent());
    return bloc;
  });
  locator.registerLazySingleton<SearchBloc>(() => SearchBloc(locator<ChuckNorrisRepository>()));
}
