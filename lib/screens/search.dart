import 'package:assignment2/bloc/search/search_bloc.dart';
import 'package:assignment2/locator.dart';
import 'package:assignment2/screens/elements/chuck_norris_joke.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({super.key});

  @override
  State createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  late final SearchBloc bloc;
  final textEditorController = TextEditingController();

  @override
  void initState() {
    super.initState();
    bloc = locator<SearchBloc>();
  }

  @override
  void dispose() {
    textEditorController.dispose();
    super.dispose();
  }

  Widget _buildBody(BuildContext context, SearchState state) {
    Widget searchResult;
    if (state is ShowSearchResultState) {
      if (state.jokes.isEmpty) {
        searchResult = const Center(child: Text("No results"));
      } else {
        searchResult = ListView(
            // padding: const EdgeInsets.only(top: 10, bottom: 10, left: 10),
            children: state.jokes
                .map(
                  (joke) => Container(
                      alignment: Alignment.topLeft,
                      margin: const EdgeInsets.only(top: 5, bottom: 5),
                      child: ChuckNorrisJokeText(text: joke.value)),
                )
                .toList());
      }
    } else if (state is LoadingSearchResultState) {
      searchResult = const Center(child: CircularProgressIndicator());
    } else {
      searchResult = Container();
    }

    return Padding(
        padding: const EdgeInsets.all(10),
        child: Column(children: [
          Padding(
              padding: const EdgeInsets.only(bottom: 10),
              child: IntrinsicHeight(
                  child: Row(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
                Expanded(
                    child: Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: TextField(
                    controller: textEditorController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Enter a search term',
                    ),
                    onSubmitted: (text) => bloc.add(MakeSearchEvent(text)),
                  ),
                )),
                ElevatedButton(
                    onPressed: () => bloc.add(MakeSearchEvent(textEditorController.text)),
                    child: const Icon(Icons.search))
              ]))),
          Expanded(child: searchResult),
        ]));
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: bloc,
      child: SafeArea(
        child: BlocBuilder<SearchBloc, SearchState>(
          builder: _buildBody,
        ),
      ),
    );
  }
}
