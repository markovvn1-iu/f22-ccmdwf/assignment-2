import 'package:flutter/material.dart';

class ChuckNorrisJokeText extends StatelessWidget {
  const ChuckNorrisJokeText({super.key, required this.text});

  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        decoration: const BoxDecoration(
            color: Color.fromRGBO(0, 0, 0, 249),
            border: Border(left: BorderSide(color: Color.fromRGBO(0, 0, 0, 210), width: 5))),
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: RichText(
              text: TextSpan(style: Theme.of(context).textTheme.bodyMedium, children: <InlineSpan>[
            const WidgetSpan(
                child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 2.0),
              child: Icon(Icons.format_quote, color: Color.fromRGBO(0, 0, 0, 210)),
            )),
            TextSpan(text: text),
          ])),
        ));
  }
}
