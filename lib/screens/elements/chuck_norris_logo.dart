import 'dart:math';

import 'package:flutter/material.dart';

class ChuckNorrisLogoController {
  ChuckNorrisLogoState? _state;

  void wiggle() async => _state?.wiggle();
}

class ChuckNorrisLogo extends StatefulWidget {
  const ChuckNorrisLogo({super.key, required this.controller, required this.image, required this.duration});

  final ChuckNorrisLogoController controller;
  final Image image;
  final Duration duration;

  @override
  State<ChuckNorrisLogo> createState() => ChuckNorrisLogoState();
}

class ChuckNorrisLogoState extends State<ChuckNorrisLogo> with SingleTickerProviderStateMixin {
  late AnimationController _logoAnimationController;
  late Animation<double> _logoAnimation;
  final GlobalKey _logoImageKey = GlobalKey();

  @override
  void initState() {
    super.initState();

    widget.controller._state = this;
    _logoAnimationController = AnimationController(vsync: this, duration: widget.duration);
    _logoAnimation = Tween<double>(begin: 0, end: 1).animate(_logoAnimationController);
  }

  @override
  void dispose() {
    _logoAnimationController.dispose();
    super.dispose();
  }

  Future wiggle() async {
    _logoAnimationController.reset();
    await _logoAnimationController.forward();
  }

  double _getLogoRotation(double x) {
    return 0.1 * sin(5 * 2 * pi * x) * (1 / (x + 1) - 0.5);
  }

  Offset _getLogoOffset() {
    final RenderBox? box = _logoImageKey.currentContext?.findRenderObject() as RenderBox?;
    if (box == null || !box.hasSize) return Offset.zero;
    return Offset(0, box.size.height / 2);
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _logoAnimation,
      child: Container(key: _logoImageKey, child: widget.image),
      builder: (context, child) =>
          Transform.rotate(angle: _getLogoRotation(_logoAnimation.value), origin: _getLogoOffset(), child: child),
    );
  }
}
