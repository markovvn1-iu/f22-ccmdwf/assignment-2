import 'package:flutter/material.dart';

class AboutPage extends StatelessWidget {
  const AboutPage({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Center(
            child: Row(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: [
      const Text("👦"),
      Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: const [
        Text("Vladimir Markov", style: TextStyle(fontWeight: FontWeight.bold)),
        Text("Email: Markovvn1@gmail.com"),
        Text("GitLab: @markovvn1")
      ])
    ])));
  }
}
