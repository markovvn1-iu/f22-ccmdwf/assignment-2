import 'package:assignment2/bloc/random_joke/random_joke_bloc.dart';
import 'package:assignment2/bloc/random_joke/random_joke_settings_bloc.dart' as settings;
import 'package:assignment2/locator.dart';
import 'package:assignment2/screens/elements/chuck_norris_joke.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';

import 'elements/chuck_norris_logo.dart';

class RandomJokePage extends StatefulWidget {
  const RandomJokePage({super.key});

  @override
  State<StatefulWidget> createState() => _RandomJokePageState();
}

class _RandomJokePageState extends State<RandomJokePage> {
  late final RandomJokeBloc bloc;
  late final settings.RandomJokeSettingsBloc settingsBloc;
  final ChuckNorrisLogoController logoController = ChuckNorrisLogoController();

  @override
  void initState() {
    super.initState();
    bloc = locator<RandomJokeBloc>();
    settingsBloc = locator<settings.RandomJokeSettingsBloc>();
  }

  void _openLink(String urlLink) async {
    final Uri uri = Uri.parse(urlLink);
    if (!await canLaunchUrl(uri)) return;
    await launchUrl(uri, mode: LaunchMode.externalApplication);
  }

  Widget _buildJokeBody(BuildContext context, RandomJokeState state) {
    if (state is InitialState || state is LoadingState) {
      return const Center(child: Padding(padding: EdgeInsets.all(40), child: CircularProgressIndicator()));
    }
    if (state is ShowJokeState) {
      return Column(children: [
        Container(
          alignment: Alignment.topLeft,
          margin: const EdgeInsets.all(20),
          child: ChuckNorrisJokeText(text: state.joke.value),
        ),
        Container(
            alignment: Alignment.centerRight,
            margin: const EdgeInsets.only(right: 20),
            child: ElevatedButton(
                onPressed: () => _openLink(state.joke.url),
                style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                ),
                child: Row(mainAxisSize: MainAxisSize.min, children: const <Widget>[
                  Text(
                    "Open in browser ",
                    style: TextStyle(color: Colors.white, fontSize: 12),
                  ),
                  Icon(Icons.open_in_browser)
                ])))
      ]);
    }
    throw Exception("Unknown state");
  }

  Future<void> _showBottomSettings(BuildContext context) {
    return showModalBottomSheet(
        context: context,
        // isScrollControlled: true,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10))),
        builder: (context) {
          return BlocProvider.value(
              value: settingsBloc,
              child: BlocBuilder<settings.RandomJokeSettingsBloc, settings.RandomJokeSettingsState>(
                builder: (context, state) {
                  if (state is settings.RandomJokeSettingsLoadedState) {
                    return ListView(
                      shrinkWrap: true,
                      padding: const EdgeInsets.all(10),
                      children: state.selectedCategories.entries
                          .map((e) => CheckboxListTile(
                              value: e.value,
                              title: Text(e.key),
                              onChanged: (v) => settingsBloc.add(settings.SelectCategoryEvent(e.key, v ?? false))))
                          .toList(),
                    );
                  }
                  return const Center(child: Padding(padding: EdgeInsets.all(40), child: CircularProgressIndicator()));
                },
              ));
        }).whenComplete(() => bloc.add(SetCategoriesEvent(settingsBloc.getSelectedCategoryList())));
  }

  Widget _buildBody(BuildContext context, RandomJokeState state) {
    final Widget favoriteButton;
    if (state is ShowJokeState) {
      favoriteButton = ElevatedButton(
          onPressed: () => bloc.add(SwitchJokeLikeEvent()),
          child: Icon(state.inFavorites ? Icons.favorite : Icons.favorite_border));
    } else {
      favoriteButton = ElevatedButton(onPressed: () {}, child: const Icon(Icons.favorite_border));
    }

    return Column(children: [
      Container(
          padding: const EdgeInsets.fromLTRB(60, 20, 60, 0),
          child: GestureDetector(
              onLongPressDown: (_) => logoController.wiggle(),
              onDoubleTap: () => _openLink("https://api.chucknorris.io/"),
              child: ChuckNorrisLogo(
                  controller: logoController,
                  duration: const Duration(milliseconds: 700),
                  image: const Image(image: AssetImage('chucknorris_logo.png'))))),
      _buildJokeBody(context, state),
      const Spacer(),
      Container(
          alignment: Alignment.bottomCenter,
          margin: const EdgeInsets.only(bottom: 40),
          child: IntrinsicHeight(
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                ElevatedButton(onPressed: () => _showBottomSettings(context), child: const Icon(Icons.settings)),
                ElevatedButton(
                    onPressed: () {
                      logoController.wiggle();
                      bloc.add(ShowNextJokeEvent());
                    },
                    style: ElevatedButton.styleFrom(
                      padding: const EdgeInsets.symmetric(horizontal: 40.0, vertical: 20.0),
                    ),
                    child: const Text(
                      "🤠 Next joke! 🤠", // button text message
                    )),
                favoriteButton,
              ]))),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: bloc,
      child: SafeArea(
        child: BlocBuilder<RandomJokeBloc, RandomJokeState>(
          builder: _buildBody,
        ),
      ),
    );
  }
}
