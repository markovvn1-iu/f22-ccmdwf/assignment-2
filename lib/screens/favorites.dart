import 'package:assignment2/bloc/favorites/favorites_bloc.dart';
import 'package:assignment2/locator.dart';
import 'package:assignment2/screens/elements/chuck_norris_joke.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FavoritePage extends StatefulWidget {
  const FavoritePage({super.key});

  @override
  State createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  late final FavoritesBloc bloc;

  @override
  void initState() {
    super.initState();
    bloc = locator<FavoritesBloc>();
    bloc.add(ShowAllJokesEvent());
  }

  Widget _buildBody(BuildContext context, FavoritesState state) {
    if (state is InitialState) {
      return const Center(child: CircularProgressIndicator());
    }
    if (state is ShowFavoritesState && state.jokes.isEmpty) {
      return const Center(child: Text("No favourites"));
    }
    if (state is ShowFavoritesState && state.jokes.isNotEmpty) {
      return ListView(
          padding: const EdgeInsets.only(top: 10, bottom: 10, left: 10),
          children: state.jokes
              .map(
                (joke) => Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(top: 5, bottom: 5),
                    child: IntrinsicHeight(
                        child: Row(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
                      Expanded(child: ChuckNorrisJokeText(text: joke.value)),
                      GestureDetector(
                          onTap: () => context.read<FavoritesBloc>().add(RemoveJokeEvent(joke.id)),
                          child: const Padding(
                              padding: EdgeInsets.only(right: 15, left: 15),
                              child: Icon(Icons.delete, color: Colors.black26))),
                    ]))),
              )
              .toList());
    }
    throw Exception("Unknown state");
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: bloc,
      child: SafeArea(
        child: BlocBuilder<FavoritesBloc, FavoritesState>(
          builder: _buildBody,
        ),
      ),
    );
  }
}
