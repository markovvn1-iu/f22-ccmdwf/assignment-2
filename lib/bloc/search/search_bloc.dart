import 'dart:async';

import 'package:assignment2/network/models/chucknorris.dart';
import 'package:assignment2/network/repository/chucknorris_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  SearchBloc(this.repository) : super(InitialState()) {
    on<MakeSearchEvent>(_makeSearch);
  }

  bool isSearching = false;
  final ChuckNorrisRepository repository;

  Future<List<ChuckNorrisJoke>> _loadSearchResult(String query) async {
    if (query.length < 3 || query.length > 120) return [];
    while (true) {
      try {
        return await repository.search(query);
      } catch (err) {
        debugPrint("$err");
        debugPrint("Failed to load search result. Try again");
      }
      await Future.delayed(const Duration(milliseconds: 1000));
    }
  }

  Future<void> _makeSearch(MakeSearchEvent e, Emitter emit) async {
    if (isSearching) return;
    isSearching = true;
    emit(LoadingSearchResultState());
    emit(ShowSearchResultState(await _loadSearchResult(e.query)));
    isSearching = false;
  }
}
