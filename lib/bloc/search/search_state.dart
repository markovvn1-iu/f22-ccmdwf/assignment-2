part of 'search_bloc.dart';

abstract class SearchState {}

class InitialState extends SearchState {}

class LoadingSearchResultState extends SearchState {}

class ShowSearchResultState extends SearchState {
  ShowSearchResultState(this.jokes);
  List<ChuckNorrisJoke> jokes;
}
