part of 'search_bloc.dart';

abstract class SearchEvent {}

class MakeSearchEvent extends SearchEvent {
  MakeSearchEvent(this.query);
  String query;
}
