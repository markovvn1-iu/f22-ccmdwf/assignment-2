import 'dart:async';

import 'package:assignment2/network/models/chucknorris.dart';
import 'package:assignment2/network/repository/joke_database_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'favorites_event.dart';
part 'favorites_state.dart';

class FavoritesBloc extends Bloc<FavoritesEvent, FavoritesState> {
  FavoritesBloc(this.jokeRepository) : super(InitialState()) {
    on<ShowAllJokesEvent>(_showAllJoke);
    on<RemoveJokeEvent>(_removeJokeEvent);
  }

  final JokeDatabaseRepository jokeRepository;

  Future<void> _showAllJoke(ShowAllJokesEvent e, Emitter emit) async {
    emit(ShowFavoritesState(await jokeRepository.getAll().toList()));
  }

  Future<void> _removeJokeEvent(RemoveJokeEvent e, Emitter emit) async {
    await jokeRepository.remove(e.id);
    await _showAllJoke(ShowAllJokesEvent(), emit);
  }
}
