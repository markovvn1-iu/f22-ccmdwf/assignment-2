part of 'favorites_bloc.dart';

abstract class FavoritesEvent {}

class ShowAllJokesEvent extends FavoritesEvent {}

class RemoveJokeEvent extends FavoritesEvent {
  RemoveJokeEvent(this.id);
  String id;
}
