part of 'favorites_bloc.dart';

abstract class FavoritesState {}

class InitialState extends FavoritesState {}

class ShowFavoritesState extends FavoritesState {
  ShowFavoritesState(this.jokes);
  List<ChuckNorrisJoke> jokes;
}
