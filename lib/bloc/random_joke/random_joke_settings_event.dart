part of 'random_joke_settings_bloc.dart';

abstract class RandomJokeSettingsEvent {}

class LoadSettingsEvent extends RandomJokeSettingsEvent {}

class SelectCategoryEvent extends RandomJokeSettingsEvent {
  SelectCategoryEvent(this.category, this.isChecked);
  String category;
  bool isChecked;
}
