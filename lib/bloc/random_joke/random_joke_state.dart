part of 'random_joke_bloc.dart';

abstract class RandomJokeState {}

class InitialState extends RandomJokeState {}

class LoadingState extends RandomJokeState {}

class ShowJokeState extends RandomJokeState {
  ShowJokeState(this.joke, this.inFavorites);
  bool inFavorites;
  ChuckNorrisJoke joke;
}
