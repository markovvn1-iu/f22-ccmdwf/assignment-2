import 'package:assignment2/network/repository/chucknorris_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'random_joke_settings_state.dart';
part 'random_joke_settings_event.dart';

class RandomJokeSettingsBloc extends Bloc<RandomJokeSettingsEvent, RandomJokeSettingsState> {
  RandomJokeSettingsBloc(this.chucknorrisRepository) : super(InitialState()) {
    on<LoadSettingsEvent>(_loadSettingsEvent);
    on<SelectCategoryEvent>(_selectCategory);
  }

  final ChuckNorrisRepository chucknorrisRepository;
  Map<String, bool>? selectedCategories;
  bool settingsLoaded = false;

  List<String> getSelectedCategoryList() {
    return selectedCategories?.entries.where((e) => e.value).map((e) => e.key).toList() ?? [];
  }

  Future<void> _emitSettings(Emitter emit) async {
    if (selectedCategories == null) return;
    emit(RandomJokeSettingsLoadedState(selectedCategories!));
  }

  Future<void> _loadSettingsEvent(LoadSettingsEvent e, Emitter emit) async {
    if (settingsLoaded) return;
    settingsLoaded = true;

    emit(LoadingState());

    selectedCategories = await (() async {
      while (true) {
        try {
          return {for (var k in await chucknorrisRepository.categories()) k: false};
        } catch (err) {
          debugPrint("$err");
          debugPrint("Failed to load categories. Try again");
        }
        await Future.delayed(const Duration(milliseconds: 1000));
      }
    })();

    await _emitSettings(emit);
  }

  Future<void> _selectCategory(SelectCategoryEvent e, Emitter emit) async {
    if (selectedCategories == null) return;
    selectedCategories![e.category] = e.isChecked;

    await _emitSettings(emit);
  }
}
