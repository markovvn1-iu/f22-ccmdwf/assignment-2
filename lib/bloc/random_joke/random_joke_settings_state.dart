part of 'random_joke_settings_bloc.dart';

abstract class RandomJokeSettingsState {}

class InitialState extends RandomJokeSettingsState {}

class LoadingState extends RandomJokeSettingsState {}

class RandomJokeSettingsLoadedState extends RandomJokeSettingsState {
  RandomJokeSettingsLoadedState(this.selectedCategories);
  Map<String, bool> selectedCategories;
}
