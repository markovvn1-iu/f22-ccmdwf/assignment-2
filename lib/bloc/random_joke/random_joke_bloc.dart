import 'dart:async';
import 'dart:collection';
import 'dart:math';

import 'package:assignment2/network/models/chucknorris.dart';
import 'package:assignment2/network/repository/chucknorris_repository.dart';
import 'package:assignment2/network/repository/joke_database_repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'random_joke_event.dart';
part 'random_joke_state.dart';

class RandomJokeBloc extends Bloc<RandomJokeEvent, RandomJokeState> {
  static const int maxPreloadedJokes = 25;

  RandomJokeBloc(this.chucknorrisRepository, this.jokeRepository) : super(InitialState()) {
    on<ShowNextJokeEvent>(_showNextJoke);
    on<SwitchJokeLikeEvent>(_switchJokeLike);
    on<SetCategoriesEvent>(_setCategories);

    for (int i = 0; i < maxPreloadedJokes; i++) {
      Completer<ChuckNorrisJoke> joke = Completer<ChuckNorrisJoke>();
      _loadedJokes.addLast(joke);
      _jokesToLoad.addLast(joke);
    }
  }

  final ChuckNorrisRepository chucknorrisRepository;
  final JokeDatabaseRepository jokeRepository;

  ChuckNorrisJoke? currentJoke;
  List<String> categories = [];
  final Queue<Completer<ChuckNorrisJoke>> _loadedJokes = Queue<Completer<ChuckNorrisJoke>>();
  final Queue<Completer<ChuckNorrisJoke>> _jokesToLoad = Queue<Completer<ChuckNorrisJoke>>();
  bool _waitingForJoke = false;
  bool _doesLoadingWorkerExist = false;
  int _nextLoadingTime = DateTime.now().millisecondsSinceEpoch;

  Future _sleepToPreventDOS(int milliseconds) async {
    int currentTime = DateTime.now().millisecondsSinceEpoch;
    int sleepFor = max(0, _nextLoadingTime - currentTime);
    debugPrint("Sleeping for $sleepFor milliseconds to prevent DOS");
    _nextLoadingTime = currentTime + sleepFor + milliseconds;
    return Future.delayed(Duration(milliseconds: sleepFor));
  }

  Future<ChuckNorrisJoke> _loadRandomJoke() async {
    debugPrint("Start loading new joke");

    while (true) {
      await _sleepToPreventDOS(1000);

      try {
        return await chucknorrisRepository.random(categories);
      } catch (err) {
        debugPrint("$err");
        debugPrint("Failed to load joke. Try again");
      }
    }
  }

  Future<void> _runLoadingWorker() async {
    if (_doesLoadingWorkerExist) return;
    _doesLoadingWorkerExist = true;

    while (_jokesToLoad.isNotEmpty) {
      final Completer<ChuckNorrisJoke> jokeCompleter = _jokesToLoad.removeFirst();
      ChuckNorrisJoke joke = await _loadRandomJoke();
      debugPrint("New joke is loaded: $joke (categories: ${joke.categories})");
      jokeCompleter.complete(joke);
    }

    _doesLoadingWorkerExist = false;
  }

  Future<void> _emitJoke(ChuckNorrisJoke joke, Emitter emit) async {
    emit(ShowJokeState(joke, await jokeRepository.has(joke.id)));
    currentJoke = joke;
  }

  Future<void> _showNextJoke(ShowNextJokeEvent e, Emitter emit) async {
    if (_waitingForJoke) return;
    _waitingForJoke = true;

    ChuckNorrisJoke? joke = currentJoke;
    if (e.loadNext || joke == null) {
      final Completer<ChuckNorrisJoke> newJoke = Completer<ChuckNorrisJoke>();
      _loadedJokes.addLast(newJoke);
      _jokesToLoad.addLast(newJoke);
      _runLoadingWorker();

      final Completer<ChuckNorrisJoke> jokeCompleter = _loadedJokes.removeFirst();
      if (!jokeCompleter.isCompleted) {
        currentJoke = null;
        emit(LoadingState());
      }
      await _emitJoke(await jokeCompleter.future, emit);
    } else {
      await _emitJoke(joke, emit);
    }

    _waitingForJoke = false;
  }

  Future<void> _switchJokeLike(SwitchJokeLikeEvent e, Emitter emit) async {
    if (_waitingForJoke) return;
    _waitingForJoke = true;

    ChuckNorrisJoke? joke = currentJoke;
    if (joke == null) return;

    if (await jokeRepository.has(joke.id)) {
      jokeRepository.remove(joke.id);
    } else {
      jokeRepository.put(joke);
    }
    await _emitJoke(joke, emit);

    _waitingForJoke = false;
  }

  Future<void> _setCategories(SetCategoriesEvent e, Emitter emit) async {
    categories = e.categories;

    _loadedJokes.clear();
    _jokesToLoad.clear();

    for (int i = 0; i < maxPreloadedJokes; i++) {
      Completer<ChuckNorrisJoke> joke = Completer<ChuckNorrisJoke>();
      _loadedJokes.addLast(joke);
      _jokesToLoad.addLast(joke);
    }

    _runLoadingWorker();
  }
}
