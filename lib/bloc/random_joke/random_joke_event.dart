part of 'random_joke_bloc.dart';

abstract class RandomJokeEvent {}

class ShowNextJokeEvent extends RandomJokeEvent {
  ShowNextJokeEvent({this.loadNext = true});
  bool loadNext;
}

class SwitchJokeLikeEvent extends RandomJokeEvent {}

class SetCategoriesEvent extends RandomJokeEvent {
  SetCategoriesEvent(this.categories);
  List<String> categories;
}
